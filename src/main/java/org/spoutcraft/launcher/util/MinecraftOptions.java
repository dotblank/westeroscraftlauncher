/*
 * This file is part of Westeroscraft Launcher.
 *
 * Copyright (c) 2011 Spout LLC <http://www.spout.org/>
 * Westeroscraft Launcher is licensed under the Spout License Version 1.
 *
 * Westeroscraft Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Spout License Version 1.
 *
 * Westeroscraft Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Spout License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://spout.in/licensev1> for the full license,
 * including the MIT license.
 */
package org.spoutcraft.launcher.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class MinecraftOptions {
	
	
	public HashMap<String,String> options;
	private File file;

	public MinecraftOptions() {
		options = new HashMap<String,String>();
	}
	
	public void load(File f) throws FileNotFoundException {
		this.file = f;
		Scanner scan = new Scanner(this.file);
		while(scan.hasNextLine()) {
			String s = scan.nextLine();
			String[] split = s.split(":", 1);
			options.put(split[0], split[1]);
		}
		scan.close();
	}
	
	public void save(File f) throws IOException {
		if(f == null) { return; }
		FileWriter fw = new FileWriter(f);
		for(Entry<String,String> e : options.entrySet())
		{
			fw.write(e.getKey() + ":" + e.getValue() + "\n");
		}
		fw.close();
	}
	public void save() throws IOException {
		this.save(file);
	}
	
}
